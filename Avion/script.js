// Global Variables
var scene, camera, renderer;
var gsize = 20;
var textload = new THREE.TextureLoader();

// Ici on retrouve toutes les textures nécéssaires pour le jeu
const bgTexture = textload.load("img/bg1.jpg"); // Background
const playerTexture = textload.load("img/plane5.png");
const bossTexture = textload.load("img/plane4.png");
const missTexture = textload.load("img/missile2.gif");
const tirTexture = textload.load("img/tir.png");
const ennemieTexture = textload.load("img/plane3.png");
const ennemieTexture2 = textload.load("img/plane6.png");

// Ici on initialise les sons qui servent dans le jeu
var missileSound = new Audio("Sounds/exploMissile.wav");
var son = new Audio("Sounds/ambiance.mp3");
var evolve = new Audio("Sounds/lvlUp.wav");
var shootSound = new Audio("Sounds/shoot1.wav");


var player; // On initialise le joueur
var playerSpeed = 0.3; // La vitesse du joueur
var botSpeed = 0.1; // La vitesse d'un bot
var bossSpeed = 0.05; // La vitesse d'un boss
var shootSpeed = 0.2; // La vitesse du projectile
var missSpeed = 0.3; // La vitesse des missiles
var damage_player = 5; // Les dégats que le joueur inflige aux ennemies
var score_player = 0;
var level_player = 0; // On initilaise le lvl et le score du joueur a 0
var life_player = 100; // La vie du joueur
var life_bot = 10; // La vie d'un bot
var life_boss = 50; // La vie d'un boss
var gain_bot = 5; // L'exp que donne un bot lorsqu'on le tue
var gain_boss = 10; // L'exp que donne un boss lorsqu'on le tue
var cptM = 0; // Le compteur qui permet de compter le nombre de missile disponible

let bots = []; // Tableau pour les ennemies
let bosss = []; // Tableau pour les boss
let shoots = []; // Tableau pour les projectiles
let missiles = []; // Tableau pour les missiles ( sur espace )
var ennemieText = []; // Tableau pour tirer aléatoirement soit la texture 1, soit la texture 2.
let keys = []; // Tableau pour les touches du clavier utilisées

var flagDmg = false; // Flag pour augmenter les dégat une seule fois par click
var flagLIfe = false; // Flag pour regen de la vie uen seule fois par click
var flagMiss = false;

var isRunning = true;


init(); // Appel de la fonction init()
animate(); // Appel de la fonction animate()
spawnEnnemies(); // Appel de la fonction spawnEnnemies()
spawnBoss(); // Appel de la fonction spawnBoss()
specialTime(); // C'est la fonction qui permet d'incrémenté la jauge d'attaque spéciale

function init() {
  scene = new THREE.Scene();
  scene.fog = new THREE.FogExp2(0xcccccc, 0.0015);
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setClearColor(scene.fog.color);
  renderer.shadowMap.enabled = true;
  renderer.setSize(window.innerWidth - 10, window.innerHeight - 20);
  document.body.appendChild(renderer.domElement);

  var axesHelper = new THREE.AxesHelper(5);
  scene.add(axesHelper);
  camera = new THREE.OrthographicCamera(-gsize, gsize, gsize, -gsize, 1, 10000);

  // Creation du plan navigable
  var plan = new THREE.PlaneGeometry(gsize, 2.5 * gsize, 1, 1);
  var matplan = new THREE.MeshBasicMaterial({ map: bgTexture });
  fond = new THREE.Mesh(plan, matplan);
  scene.add(fond);
  // Creation du cube joueur
  var geometry = new THREE.BoxGeometry(1, 3, 1);
  var material = new THREE.MeshBasicMaterial({
    map: playerTexture,
    transparent: true
  });
  player = new THREE.Mesh(geometry, material);
  player.position.y = -10;
  scene.add(player);

  camera.position.set(0, 5, 20);
  scene.add(camera);
  addLights();

  // On initialise les fonction key et nokey à false
  document.addEventListener("keydown", key, false);
  document.addEventListener("keyup", nokey, false);
  document.addEventListener("keydown", ondocKeydown, false);
  son.play();
  son.loop = true;
  son.volume = 0.3;
}

function animate() {
  if (isRunning) var anime = requestAnimationFrame(animate);

  movePlayer(); // C'est ici qu'on appelle la fonction pour faire déplacer le joueur
  shootMisille(); // Ici on appelle la fonction pour générer les missiles
  mute();
  pause();
  question();

  bots.forEach(function(bot) {
    bot.position.y -= botSpeed; // Déplacements de ennemies
    Object.values(shoots).forEach(function(shoot) {
      if (shoot.position.y > 25) {
        shoots.splice(shoots.indexOf(shoot), 1); // On enlève du tableau le projectile qui dépasse la position souhaité.
        scene.remove(shoot); // On l'enlève de la scène
      }
      if (
        shoot.position.y >= bot.position.y - 0.2 &&
        shoot.position.y <= bot.position.y + 0.2 &&
        shoot.position.x >= bot.position.x - 0.8 &&
        shoot.position.x <= bot.position.x + 0.8
      ) {
        // Si le projectile touche un ennemie
        shoots.splice(shoots.indexOf(shoot), 1); // On enlve aussi le projectile qu'il l'a touché
        scene.remove(shoot);
        life_bot -= damage_player;
        if (life_bot <= 0) {
          score_player += 10;
          document.getElementById("score").innerHTML = "Score: " + score_player;
          bots.splice(bots.indexOf(bot), 1); // on enleve un ennemie du tableau
          scene.remove(bot); // Et on l'enlève de la scène
          life_bot = 10;
          var level = document.getElementById("oui2");
          if (level_player >= 100 - gain_bot) {
            level.style.width = 100 + "%";
          } else if (level_player < 100 - gain_bot) {
            level_player += gain_bot;
            level.style.width = level_player + "%";
          }
        }
      }
    });
    if (bot.position.y < -14) {
      // Si le bot sort de l'écran vers le bas, on le détruit
      bots.splice(bots.indexOf(bot), 1);
      scene.remove(bot);
      var life = document.getElementById("oui");
      if (life_player > 9) life_player -= 10;
      else if (life_player <= 9) life_player -= life_player;
      life.style.width = life_player + "%";
      if (life_player <= 0) {
        var over = document.getElementById("gameover");
        over.style.display = "block";
        window.cancelAnimationFrame(anime);
      }
    }
    if (
      bot.position.y >= player.position.y - 0.9 &&
      bot.position.y <= player.position.y + 0.9 &&
      bot.position.x >= player.position.x - 0.9 &&
      bot.position.x <= player.position.x + 0.9
    ) {
      // Si le bot touche le joueur, alors on le détruit également
      bots.splice(bots.indexOf(bot), 1);
      scene.remove(bot);

      var life = document.getElementById("oui");
      life_player -= 10;
      console.log(life_player);
      life.style.width = life_player + "%";
      if (life_player <= 0) {
        var over = document.getElementById("gameover");
        over.style.display = "block";
        window.cancelAnimationFrame(anime);
      }
    }
    Object.values(missiles).forEach(function(miss) {
      if (
        miss.position.y >= bot.position.y - 0.15 &&
        miss.position.y <= bot.position.y + 0.15 &&
        miss.position.x >= bot.position.x - 0.8 &&
        miss.position.x <= bot.position.x + 0.8
      ) {
        missiles.splice(missiles.indexOf(miss), 1);
        scene.remove(miss);
        bots.splice(bots.indexOf(bot), 1);
        scene.remove(bot);
        missileSound.play();
        score_player += 10;
        document.getElementById("score").innerHTML = "Score: " + score_player;
        life_bot = 10;

        var level = document.getElementById("oui2");
        if (level_player >= 100 - gain_bot) {
            level.style.width = 100 + "%";
          } else if (level_player < 100 - gain_bot) {
            level_player += gain_bot;
            level.style.width = level_player + "%";
          }
      }
      if (miss.position.y > 25) {
        missiles.splice(missiles.indexOf(miss), 1);
        scene.remove(miss);
      }
    });
  });

  // On gère les collisions avec les boss
  bosss.forEach(function(boss) {
    boss.position.y -= bossSpeed;
    Object.values(shoots).forEach(function(shoot) {
      if (shoot.position.y > 25) {
        shoots.splice(shoots.indexOf(shoot), 1); // On enlève du tableau le projectile qui dépasse la position souhaité.
        scene.remove(shoot); // On l'enlève de la scène
      }
      if (
        shoot.position.y >= boss.position.y - 1 &&
        shoot.position.y <= boss.position.y + 1 &&
        shoot.position.x >= boss.position.x - 1 &&
        shoot.position.x <= boss.position.x + 1
      ) {
        // Si le projectile touche un boss
        shoots.splice(shoots.indexOf(shoot), 1); // On enlve aussi le projectile qu'il l'a touché
        scene.remove(shoot);
        life_boss -= damage_player;

        if (life_boss <= 0) {
          score_player += 100; // On augmente le score du joueur
          document.getElementById("score").innerHTML = "Score: " + score_player; // On modifie la valeur du score dans le code html
          bosss.splice(bosss.indexOf(boss), 1); // on enleve un ennemie du tableau
          scene.remove(boss); // Et on l'enlève de la scène
          life_boss = 50; // On remet la valeur de la vie du boss à son état initial
          var level = document.getElementById("oui2");
          if (level_player >= 100 - gain_boss) {
            console.log(level_player);
            console.log(gain_boss);
            level.style.width = 100 + "%";
          } else if (level_player < 100 - gain_boss) {
            level_player += gain_boss;
            level.style.width = level_player + "%";
          }
        }
      }
    });
    if (boss.position.y < -14) {
      // Si le boss sort de l'écran vers le bas, on le détruit
      bosss.splice(bosss.indexOf(boss), 1);
      scene.remove(boss);
      var life = document.getElementById("oui");
      if (life_player > 9) life_player -= 10;
      else if (life_player <= 9) life_player -= life_player;
      life.style.width = life_player + "%";
      if (life_player <= 0) {
        var over = document.getElementById("gameover");
        over.style.display = "block";
        window.cancelAnimationFrame(anime);
      }
    }
    if (
      boss.position.y >= player.position.y - 3 &&
      boss.position.y <= player.position.y + 3 &&
      boss.position.x >= player.position.x - 1.5 &&
      boss.position.x <= player.position.x + 1.5
    ) {
      // Si le boss touche le joueur, alors on le détruit également
      bosss.splice(bosss.indexOf(boss), 1);
      scene.remove(boss);

      var life = document.getElementById("oui");
      if (life_player > 49) {
        life_player -= 50;
      } else if (life_player <= 49) {
        life_player -= life_player;
      }
      life.style.width = life_player + "%";
      if (life_player <= 0) {
        var over = document.getElementById("gameover");
        over.style.display = "block";
        window.cancelAnimationFrame(anime);
      }
    }
    Object.values(missiles).forEach(function(miss) {
      if (
        miss.position.y >= boss.position.y - 1 &&
        miss.position.y <= boss.position.y + 1 &&
        miss.position.x >= boss.position.x - 1 &&
        miss.position.x <= boss.position.x + 1
      ) {
        missiles.splice(missiles.indexOf(miss), 1);
        scene.remove(miss);
        bosss.splice(bosss.indexOf(boss), 1);
        scene.remove(boss);
        missileSound.play();
        score_player += 100; // On augmente le score du joueur
        document.getElementById("score").innerHTML = "Score: " + score_player; // On modifie la valeur du score dans le code html
        life_boss = 50; // On remet la valeur de la vie du boss à son état initial
        var level = document.getElementById("oui2");
        if (level_player >= 100 - gain_boss) {
          level.style.width = 100 + "%";
        } else if (level_player < 100 - gain_boss) {
          level_player += gain_boss;
          level.style.width = level_player + "%";
        }
      }
      if (miss.position.y > 25) {
        missiles.splice(missiles.indexOf(miss), 1);
        scene.remove(miss);
      }
    });
  });

  // Déffilement automatique du background pour un effet d'avancement
  bgTexture.wrapT = THREE.RepeatWrapping;
  bgTexture.offset.y += 0.001;

  // Ici on gère les déplacements des projectiles.
  shoots.forEach(function(shoot) {
    shoot.translateY(shootSpeed);
  });

  missiles.forEach(function(miss) {
    miss.translateY(missSpeed);
  });

  var level = document.getElementById("oui2");
  if (level.style.width == 100 + "%") {
    evolve.play();
    evolve.volume = 1;
    level_player = 0;
    level.style.width = level_player + "%";
    if (gain_bot > 2) gain_bot--;
    if (gain_boss > 3) gain_boss--;
    levelUp();
    flagDmg = true;
    flagLIfe = true;
    var btngrp = document.getElementById("grpbtn");
    if (btngrp.style.display == "none") {
      btngrp.style.display = "block";
    }
  }

  var special = document.getElementById("oui3");
  var nbMiss = document.getElementById("nbMiss");
  if (special.style.width == 100 + "%") {
    special.style.width = 0 + "%";
    specialTime();
    cptM++;
    nbMiss.innerHTML = ": " + cptM;
  }

  if (score_player > 2000) {
    botSpeed = 0.15;
    bossSpeed = 0.05;
  }
  if (score_player > 5000) {
    botSpeed = 0.3;
    bossSpeed = 0.08;
  }
  renderer.render(scene, camera);
}

function addLights() {
  // Merci à monsieur Omidvar pour les lumières
  var lightOne = new THREE.DirectionalLight(0xffffff);
  lightOne.position.set(1, 1, 1);
  scene.add(lightOne);
  // Add a second light with half the intensity
  var lightTwo = new THREE.DirectionalLight(0xffffff, 0.5);
  lightTwo.position.set(1, -1, -1);
  scene.add(lightTwo);
}

// Pression de keys
function key(e) {
  keys[e.keyCode] = true;
}
// Relachement de keys
function nokey(e) {
  keys[e.keyCode] = false;
}
// Fonction pour les déplacements du joueur
function movePlayer() {
  if (keys[90]) {
    // MOVE UP
    if (player.position.y < 24) {
      player.translateY(playerSpeed);
      if (keys[68] && player.position.x < 9.5) {
        // MOVE UP AND RIGHT
        player.translateX(playerSpeed);
      }
      if (keys[81] && player.position.x > -9.5) {
        // MOVE UP AND LEFT
        player.translateX(-playerSpeed);
      }
    }
  } else if (keys[83]) {
    // MOVE DOWN
    if (player.position.y > -14) {
      player.translateY(-playerSpeed);
      if (keys[68] && player.position.x < 9.5) {
        // MOVE DOWN AND RIGHT
        player.translateX(playerSpeed);
      }
      if (keys[81] && player.position.x > -9.5) {
        // MOVE DOWN AND LEFT
        player.translateX(-playerSpeed);
      }
    }
  } else if (keys[81]) {
    // MOVE LEFT
    if (player.position.x > -9.5) player.translateX(-playerSpeed);
  } else if (keys[68]) {
    // MOVE RIGHT
    if (player.position.x < 9.5) player.translateX(playerSpeed);
  }
}

// Fonction pour faire les ennemies
function spawnEnnemies() {
  var botx = Math.floor(Math.random() * 18.5 - 9); // Position random sur l'axe des x
  var txture = Math.floor(Math.random() * Math.floor(2)); // Random soit 1 soit 0
  var geometry = new THREE.BoxGeometry(1, 2, 1);
  var material = new THREE.MeshBasicMaterial({
    map: ennemieTexture,
    transparent: true
  });
  var material2 = new THREE.MeshBasicMaterial({
    map: ennemieTexture2,
    transparent: true
  });
  ennemieText.push(material);
  ennemieText.push(material2);

  bot = new THREE.Mesh(geometry, ennemieText[txture]);
  bot.rotation.x = Math.PI;
  scene.add(bot);
  bot.position.set(botx, 23, 0);
  setTimeout("spawnEnnemies()", 1000);
  bots.push(bot); // On ajoute le bot dans le tableau 'bots' pour une gestion plus facile
  bots.life = 10;
}

// Fonction pour faire les boss
function spawnBoss() {
  var bossx = Math.floor(Math.random() * 18.5 - 9);
  var geoBoss = new THREE.BoxGeometry(2, 4, 2);
  var matBoss = new THREE.MeshBasicMaterial({
    map: bossTexture,
    transparent: true
  });
  boss = new THREE.Mesh(geoBoss, matBoss);
  scene.add(boss);
  boss.rotation.x = Math.PI;
  boss.position.set(bossx, 23, 0);
  setTimeout("spawnBoss()", 10000);
  bosss.push(boss);
}

// Fonction pour faire les projectiles
function spawnShoots() {
  var geo = new THREE.PlaneGeometry(0.8, 1.6, 0.8);
  var mat = new THREE.MeshBasicMaterial({ map: tirTexture, transparent: true });
  shoot = new THREE.Mesh(geo, mat);
  scene.add(shoot);
  shoot.position.set(player.position.x, player.position.y, 0);
  shoots.push(shoot); // On ajoute les projectils dans le tableau 'shoots' pour une gestion plus facile
  var shootSound = new Audio("Sounds/shoot1.wav");
  shootSound.play();
  shootSound.volume = 0.1;
}

// Fonction pour faire les missiles ( espace )
function shootMisille() {
  if (keys[32] && cptM > 0 && flagMiss == false) {
    var nbMiss = document.getElementById("nbMiss");
    cptM--;
    nbMiss.innerHTML = ": " + cptM;
    flagMiss = true;
    setTimeout("flagMissTrue()", 500);
    const geoM = new THREE.PlaneGeometry(1, 2, 1);
    const matM = new THREE.MeshBasicMaterial({
      map: missTexture,
      transparent: true
    });
    miss = new THREE.Mesh(geoM, matM);
    scene.add(miss);
    miss.position.set(player.position.x, player.position.y, 0);
    missiles.push(miss); // On ajoute les projectils
  }
}

// Fonction pour créer le bouton pour augmenter les dégats
function createButton1() {
  var button1 = document.createElement("button");
  button1.innerHTML = "Upgrade damages";
  document.getElementById("grpbtn").appendChild(button1);
  button1.setAttribute("id", "btnDmg");
  var dmg = document.getElementById("btnDmg");
  dmg.classList.add("btn");
  dmg.classList.add("btn-danger");
  dmg.onclick = function() {
    increaseDmg();
  };
}

// Fonction pour créer le bouton pour régénerer de la vie
function createButton2() {
  var button2 = document.createElement("button");
  button2.innerHTML = "Heal 10HP";
  document.getElementById("grpbtn").appendChild(button2);
  button2.setAttribute("id", "btnLife");
  var blf = document.getElementById("btnLife");
  blf.classList.add("btn");
  blf.classList.add("btn-info");
  blf.onclick = function() {
    increaseLife();
  };
}

// La fonction qui est appliqué au bouton1
function increaseDmg() {
  damage_player *= 1.2;
  console.log(damage_player);

  var btngrp = document.getElementById("grpbtn");
  btngrp.style.display = "none";
}

// La fonction qui est appliqué au bouton 2
function increaseLife() {
  if (life_player < 90) {
    life_player += 10;
    document.getElementById("oui").style.width = life_player + "%";
  } else if (life_player >= 90) {
    life_player = 100;
    document.getElementById("oui").style.width = 100 + "%";
  }
  var btngrp = document.getElementById("grpbtn");
  btngrp.style.display = "none";
}

// La focntion qui permet de créer les boutons une fois qu'on est lvl up
function levelUp() {
  if (flagDmg == false && flagLIfe == false) {
    createButton1();
    createButton2();
    console.log("block");
    var grpbtn = document.getElementById("grpbtn");
    grpbtn.style.display = "block";
  }
}

// Fonction pour incrémenter la bar de spécial au fur et à mesure du temps
function specialTime() {
  var i = 0;
  var incrementeBar = setInterval(function() {
    i++;
    if (i < 100) {
      $(".bg-info").css("width", 5 * i + "%");
    } else {
      clearInterval(incrementeBar);
    }
  }, 3000);
}

function flagMissTrue() {
  if (flagMiss == true) {
    flagMiss = false;
  }
}

function ondocKeydown(event) {
  var keyCode = event.which;
  if (keyCode == 82) {
    if (life_player <= 0) {
      document.location.reload();
    }
  }
}

function mute() {
  var vol = document.getElementById("volume");
  var mute = document.getElementById("mute");
  mute.onclick = function() {
    mute.style.display = "none";
    vol.style.display = "block";
    son.volume = 0;
  };
  vol.onclick = function() {
    vol.style.display = "none";
    mute.style.display = "block";
    son.volume = 0.3;
  };
}

function togglePause() {
  isRunning = !isRunning;
  if (isRunning) animate();
}

function pause() {
  var play = document.getElementById("play");
  var pause = document.getElementById("pause");
  pause.onclick = function() {
    togglePause();
    pause.style.display = "none";
    play.style.display = "block";
  };
  play.onclick = function() {
    togglePause();
    play.style.display = "none";
    pause.style.display = "block";
  };
}

function question(){
  var quest = document.getElementById("question");
  var howto = document.getElementById("howto");
  quest.onclick = function(){
    if(howto.style.display == "none"){
      howto.style.display = "block";
    }
    else 
      howto.style.display = "none";
  }
}
